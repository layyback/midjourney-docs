<font color="#9ab0fc" size="4">您可以在 Discord 上与 Midjourney Bot 互动。 Midjourney 服务器具有用于协作、技术和计费支持、官方公告、提供反馈和讨论的渠道。社区是支持、鼓励的，并渴望与刚刚起步的用户分享他们的知识。</font>

### Midjourney Discord: [https://discord.gg/midjourney](https://discord.gg/midjourney)

---

## Discord 界面

![image.png](https://i.midjourney.xin//14c570e4cdd6eec85777fc089caed5f9.png)

|

### Server List 服务器列表

![image.png](https://i.midjourney.xin//16724e9260cbcea9f9904cf66e841cc9.png)
**Direct Messages**
在直接消息中与中途机器人进行一对一合作，以获得更安静的体验。

**Midjourney Official Server**
官方中程服务器，提供协作和计费/技术支持渠道。 |

### Channel List 频道列表

![image.png](https://i.midjourney.xin//6434590a78dcf21d1b19dec154f1c508.png)
**#member-support**
请访问此频道以获得中途指南的计费和技术支持。

**#newbies**
访问任何#newbies 频道来创建图像。 |

### Member List 会员名单

![image.png](https://i.midjourney.xin//7a9e36043582a9b94d62dc20ff6211ca.png)
**Midjourney Bot **
使用 `/imagine` 命令生成图像的机器人

**Moderators and Guides **
版主和指南可以帮助解决支持渠道中的计费和技术问题。 |
| --- | --- | --- |

---

## Image Grids 图像网格

`/imagine` 命令根据您的提示生成低分辨率图像选项网格。使用每个图像网格下方的按钮创建图像的变体、升级图像或重新运行最后一个 Midjourney Bot 操作。

| ![image.png](https://i.midjourney.xin//a2c285724af96159395bb363ba491a4c.png) |

### 档位按钮

`U1` `U2` `U3` `U4` U 按钮放大图像，生成所选图像的更大版本并添加更多细节。

### 重做

** 🔄** 重做（重新滚动）按钮重新运行作业。在这种情况下，它将重新运行原始提示，生成新的图像网格。

### 变化按钮

| `V1` `V2` `V3` `V4` V 按钮创建所选网格图像的增量变化。创建变体会生成与所选图像的整体风格和构图相似的新图像网格。 |
| ---------------------------------------------------------------------------------------------------------------- |

---

## 放大图像

| ![image.png](https://i.midjourney.xin//4c31f153cc2e0617528a77a76af94739.png) | `🪄 Make Variations` `Web ↗️` `❤️ Favorite`
**制作变体：**创建放大图像的变体并生成包含四个选项的新网格。
**网页：**在 Midjourney.com 上打开图库中的图像
**最喜欢的：**标记您最好的图像，以便在 Midjourney 网站上轻松找到它们。 |
| --- | --- |

---

## 直接消息

如果#general 或#newbie 频道进展太快，Midjourney 订阅者可以在其 Discord 直接消息中与 Midjourney 机器人进行一对一合作。
了解如何向 Midjourney Bot 直接发送消息

---

## 表情符号反应

对中途作业使用不同的表情符号进行反应，将图像发送到您的私信、取消正在进行的作业或删除图像。
[了解如何使用表情符号反应。](https://docs.midjourney.com/discord-emoji-reactions)

---

## 每日主题频道

在#daily-theme 频道中参与有趣的主题团体图像生成。查找频道名称旁边的当天主题。所有世代都必须包含当天的关键词之一。
![image.png](https://i.midjourney.xin//edb041c8235f24a330bf279108ad9aff.png)

> **关闭每日主题通知**
> 想要避免来自每日主题频道的每日通知吗？使用 `/daily_theme` 命令关闭该频道的通知。
