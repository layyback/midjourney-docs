<font color="#9ab0fc" size="4">最新的 Midjourney 模型版本 5（和 Niji 5）可生成高分辨率 1024 x1024 像素图像网格，无需额外的步骤来升级每个图像。使用最新的中途模型时，每个图像网格下的 `U1` `U2` `U3` `U4` 按钮会将所选图像与初始图像网格分开以便于保存和下载。</font>

---

## Midjourney 尺寸参数和大小

_所有尺寸均适用于长宽比为 1:1 的正方形。_

| **Model Version 型号版本** | **起始网格图像大小**  | **模型版本 4 默认放大器** |
| -------------------------- | --------------------- | ------------------------- |
| Version 5                  | 1024 x 1024 1024×1024 | -                         |
| Version 4                  | 512 x 512 512×512     | 1024 x 1024 1024×1024     |
| niji 5                     | 1024 x 1024 1024×1024 | -                         |
| niji 4                     | 512 x 512 512×512     | 1024 x 1024 1024×1024     |

[DPI.了解有关图像大小、尺寸和 DPI 的更多信息。](https://docs.midjourney.com/image-size)

### 传统 Midjourney 放大器

早期的 Midjourney 版本首先为每个作业生成低分辨率图像选项网格。您可以在任何这些图像上使用 Midjourney upscaler 来增加尺寸并添加其他细节。使用升级程序会占用您订阅的 GPU 分钟数。
[了解有关旧版升级选项的更多信息。](https://docs.midjourney.com/legacy/docs/upscalers)

---

---

## 型号版本 4 升级版

Midjourney 模型版本 4 的放大器增加了图像尺寸，同时平滑和细化细节。一些小元素可能会在初始网格图像和最终的高档图像之间发生变化。

|

##### prompt: `adorable rubber duck medieval knight`

提示词： `adorable rubber duck medieval knight`
![image.png](https://i.midjourney.xin//34c291403dfb67ce35161ed257c2e8d3.png) |

##### prompt: `sand cathedral`

提示词： `sand cathedral`
![image.png](https://i.midjourney.xin//15f1849ec17d80680896f568f78c666c.png) |
| --- | --- |

---

## 重制版

重制版是使用以前的 Midjourney 模型版本制作的升级图像的一个选项。 Remaster 将使用 `--v 5.1` 参数创建新的图像网格，混合原始图像的组成和新 `-v 5.1` 模型的一致性。
通过单击原始升级下方的 `🆕 Remaster` 按钮重新掌握之前升级的任何作业。
要重新管理非常旧的作业，请使用 `/show` 命令在 Discord 中刷新该作业。

|

##### 原始模型 版本 2 图像

![image.png](https://i.midjourney.xin//3d42982ffd28c9ac783b8ac4b385164c.png) |

##### 使用模型 版本 5.1 重新制作

| ![image.png](https://i.midjourney.xin//a256c32af71e10a7d7b27f0360f4ab84.png) |
| ---------------------------------------------------------------------------- |
