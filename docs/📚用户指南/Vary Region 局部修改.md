<font color="#9ab0fc" size="4">使用 Midjourney Vary Region 编辑器选择并重新生成放大图像的特定部分。</font>
:::info
此工具要求您使用最新的 Discord 客户端版本，如果您没有看到“更改区域”按钮，请尝试更新您的 Discord 客户端。
:::

`Vary (Region)` 按钮会在中途图像放大后出现。
区域差异由原始图像中的内容和您选择的区域决定。
`Vary (Region)` 与中途模型版本 `V5.0` 、 `V5.1` 、 `V5.2` 和 `niji 5` 兼容

---

## 如何使用不同区域

### 1. 生成图像

使用 **/imagine** 命令创建图像。
![](https://i.midjourney.xin//ff49e400bb31affe72c907a976e72e2a.gif)

### 2. 升级图像

使用 U 按钮放大所选图像。
![image.png](https://i.midjourney.xin//897de24289c950208f67d0eccf84051e.png)

### 3. 选择不同区域

点击 `🖌️ Vary (Region)` 按钮打开编辑界面。
![image.png](https://i.midjourney.xin//eb431ff293ee12cbfa4cfd8dd91428fe.png)

### 4. 选择要再生的区域

- 选择编辑器左下角的手绘或矩形选择工具。
- 选择要重新生成的图像区域。
  - 您选择的大小将影响您的结果。更大的选择为中途机器人提供了更多空间来生成新的创意细节。较小的选择将导致更小、更微妙的变化。
  - 注意：您无法编辑现有选择，但可以使用右上角的撤消按钮撤消多个步骤。

![image.png](https://i.midjourney.xin//37020d9790a5df7ffd89cd54aaf52014.png)

### 5. 提交您的工作

单击 `Submit →` 按钮将您的请求发送到 Midjourney Bot。现在可以关闭 Vary Region 编辑器，并且在处理作业时您可以返回 Discord。
注意 您可以多次使用放大图像下方的 `🖌️ Vary (Region)` 按钮来尝试不同的选择。您之前的选择将被保留。您可以继续添加到此现有选择或使用 `undo` 按钮清除您的选择。
![image.png](https://i.midjourney.xin//2e71ea462bd4080a099e338701c6587c.png)

### 6. 查看结果

中途机器人将处理您的作业并在您选择的区域内生成一个新的变化图像网格。
![image.png](https://i.midjourney.xin//5be18277c46e5da91c83e4d14ee69985.png)

---

## 局部修改的示例

| scaled Image 放大图像
![image.png](https://i.midjourney.xin//356f13c16629cce4fb636548b8655fc0.png)
Prompt: colorful candy brooches
提示词：彩色糖果胸针 | Selection 选择
![image.png](https://i.midjourney.xin//3ecc20d4aa5f8dba4a723343a61e86e2.png) | Result 结果
![image.png](https://i.midjourney.xin//3bd0b50756f7a3f9c422b7f3154fbdfb.png) |
| --- | --- | --- |

| Upscaled Image 放大图像
![image.png](https://i.midjourney.xin//8b8369288634a8c6424023597352c815.png)
Prompt: architectural drawing of a house
提示词：房屋建筑图 | Selection 选择
![image.png](https://i.midjourney.xin//9608f07bf0b858af4d3caaf9e737f569.png) | Result 结果
![image.png](https://i.midjourney.xin//2d7497b641e84379e9402476401d0083.png) |
| --- | --- | --- |

---

## Vary Region + Remix Mode

局部修改 + 混音模式
您可以将重新混合模式与“不同区域”编辑器结合使用，以更新提示，同时重新生成图像的特定部分。了解如何将混音模式与 Vary Region 编辑器结合使用。

| Original Image 原始图像
![image.png](https://i.midjourney.xin//5a4ed81ec80d2794768876d5a6650168.png)
Prompt： `gouache`\*\* \*\*`alligator in sunglasses` | Selection 选择
![image.png](https://i.midjourney.xin//b0ad93f9cbc3d0dbe9be555670e0189f.png)
更新的提示词： `gouache alligator in green sunglasses` | Result 结果
![image.png](https://i.midjourney.xin//e0e067520e21f6b0b53d8fbc5f710ad1.png)
更新的提示词： `gouache alligator in green sunglasses` |
| --- | --- | --- |

---

## 技术细节

**使用 Very（区域）生成的作业将遵循以下参数：**
--chaos - 混乱
--fast - 快速地
--iw
--no - 不
--stylize --风格化
--relax - 放松
--style - 风格
--version - 版本
--video - 视频
--weird - 诡异的
:::
