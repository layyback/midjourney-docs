<font color="#9ab0fc" size="4">使用每个图像网格下的 `V1` `V2` `V3` `V4` 按钮创建生成图像的微妙或强烈变化，或使用 `Vary (Strong)` 和 `Vary (Subtle)` 按钮。</font>
使用 `🎨 High Variation Mode` 和 `🎨 Low Variation Mode` 设置控制使用这些按钮创建的变化量。您还可以使用放大图像下显示的 `🪄 Vary (Strong)` 或 `🪄 Vary (Subtle)` 按钮创建放大图像的变体。 `🪄 Vary (Region)` 修改生成图像中的选定区域，而不更改整个图像。了解有关不同地区的更多信息

> `🎨 High Variation Mode` 和 `🎨 Low Variation Mode` 影响混音模式
> `🎨 High Variation Mode` 是默认设置。
> `🪄 Vary (Strong)` 和 `🪄 Vary (Subtle)` 适用于型号版本 5.2 和 Niji 5。旧型号有一个 `🪄 Make Variations` 按钮。

---

## 变化比较

对于 `🎨 High Variation Mode` 和 `🪄 Vary (Strong)` ，使用“变化”按钮将生成一个新图像，该图像可能会更改图像中的构图、元素数量、颜色和细节类型。高变化模式对于基于单个生成的图像创建多个概念非常有用。
`🎨 Low Variation Mode` 和 `🪄 Vary (Subtle)` 生成的变体保留了原始图像的主要构图，但对其细节进行了细微的更改。此模式有助于细化或对图像进行细微调整。

提示示例： `imagine/ prompt` `mosaic estuary`

|

##### 原始图像

![image.png](https://i.midjourney.xin//deffbb68be5f8e642efac5bdbda91205.png) |

##### 使用 `🪄Vary (Subtle)` 的图像网格

![image.png](https://i.midjourney.xin//c69c7ee974a6abdd81564dec73f1f557.png) |

##### 使用 `🪄Vary (Strong)` 的图像网格

| ![image.png](https://i.midjourney.xin//0da05383aa1f7786f659cb65b8c52925.png) |
| ---------------------------------------------------------------------------- |

提示示例： `imagine/ prompt` `Murrine glass vegetables`

|

##### 原始图像

![image.png](https://i.midjourney.xin//51bbaa4e163e8fc091ffcfa2c6b78067.png) |

##### 使用 `🪄Vary (Subtle)` 的图像网格

![image.png](https://i.midjourney.xin//f5a844e02e1bae0356bebb1da4d77107.png) |

##### 使用 `🪄Vary (Strong)` 的图像网格

| ![image.png](https://i.midjourney.xin//fda84e4cf089d373b8a620648029ba76.png) |
| ---------------------------------------------------------------------------- |

---

## 如何改变 变化量

### 使用变化按钮

使用放大图像下的 `🪄 Vary (Strong)` 或 `🪄 Vary (Subtle)` 按钮可创建该图像的强烈或微妙变化。

### 使用设置命令

使用 `/settings` 命令并从菜单中选择 `🎨 High Variation Mode` 或 `🎨 Low Variation Mode` 来设置使用 `V1` 时或多或少变化的结果的首选项 `V2` `V3` `V4` 按钮。
