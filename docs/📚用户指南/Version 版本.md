<font color="#9ab0fc" size="4">Midjourney 定期发布新模型版本，以提高效率、一致性和质量。默认为最新型号，但可以通过添加 --version 或 --v 参数或使用 /settings 命令并选择型号版本来使用其他型号。每个模型都擅长生成不同类型的图像。</font>

> `--version` 接受值 1、2、3、4、5、5.1 和 5.2
> `--version` 可以缩写为 `--v`
> `--v 5.2` 是当前的默认模型。

---

## 模型版本 5.2

Midjourney V5.2 模型是最新、最先进的模型，于 2023 年 6 月发布。要使用此模型，请将 **--v 5.2** 参数添加到提示符末尾，或使用 **/settings** 命令并选择 **5️⃣ MJ Version 5.2**
默认型号 06/22/23–当前
该模型可产生更详细、更清晰的结果以及更好的颜色、对比度和构图。与早期模型相比，它对提示的理解也稍好一些，并且对 **--stylize** 参数的整个范围的响应更加灵敏。

| ![image.png](https://i.midjourney.xin//9a122a3260116fe5edc10b5fefccd4cc.png)
**_Prompt: vibrant California poppies --v 5.2
提示词：充满活力的加州罂粟花 --v 5.2_** | ![image.png](https://i.midjourney.xin//d1be46a9a82d6baff84cc0347c92005e.png)
**_Prompt: high contrast surreal collage --v 5.2
提示词：高对比度超现实拼贴--v 5.2_** |
| --- | --- |

### 模型版本 5.2 + 样式原始参数

中途模型版本 5.1 和 5.2 可以使用 `--style raw` 参数进行微调，以减少中途默认美感。
[了解有关中途--style 参数的更多信息。](https://docs.midjourney.com/style)

|

##### **default --v 5.2**

![image.png](https://i.midjourney.xin//f5ee12a02d4eb3a4b7928d48b07b499d.png)
vibrant California poppies
充满活力的加州罂粟花 |

##### **--v 5.2 --style raw**

![image.png](https://i.midjourney.xin//f9fdc058c31c6d32fd447efd8d717700.png)
vibrant California poppies --style raw
充满活力的加州罂粟花——风格生 |

##### **default --v 5.2**

![image.png](https://i.midjourney.xin//418b83aa9836f593c3b0af12ebf2f464.png)
high contrast surreal collage
高对比度超现实拼贴 |
| --- | --- | --- |
|

##### **--v 5.2 --style raw**

![image.png](https://i.midjourney.xin//44cae1646b19fbe43c35c7e558b1fd7d.png)
high contrast surreal collage --style raw
高对比度超现实拼贴画--raw 风格 | | |

---

## 模型版本 5.1

Midjourney V5.1 于 2023 年 5 月 4 日发布。要使用此模型，请将 `--v 5.1` 参数添加到提示符末尾，或使用 `/settings` 命令并选择 `5️⃣ MJ Version 5.1`
默认型号 05/03/23–06/22/23
该模型比早期版本具有更强的默认美感，使其更易于使用简单的文本提示。它还具有高一致性，擅长准确解释自然语言提示，产生更少的不需要的伪影和边框，提高了图像清晰度，并支持使用 `--tile` 重复模式等高级功能。

| ![image.png](https://i.midjourney.xin//d55646a2e4980ce42de6328d36f4b23c.png)
**_Prompt: vibrant California poppies --v 5.1
提示：充满活力的加州罂粟花 --v 5.1_** | ![image.png](https://i.midjourney.xin//fc8c92766061ed38ee6e6f7b57ea8c98.png)
**_Prompt: high contrast surreal collage --v 5.1
提示：高对比度超现实拼贴--v 5.1_** |
| --- | --- |

---

## 模型版本 5.0

Midjourney V5.0 型号比 V5.1 型号产生更多的摄影世代。该模型生成的图像与提示非常匹配，但可能需要更长的提示才能实现您所需的美感。
默认型号 03/30/23–05/03/23
要使用此模型，请将 `--v 5` 参数添加到提示末尾，或使用 `/settings` 命令并选择 `5️⃣ MJ Version 5`

| ![image.png](https://i.midjourney.xin//98064ef6fd9841e8fa0e6ff6dadd74ef.png)
**_Prompt: vibrant California poppies --v 5
提示：充满活力的加州罂粟花 --v 5_** | ![image.png](https://i.midjourney.xin//161e3b3fdb309a684c806f8dbbd779c1.png)
**_Prompt: high contrast surreal collage --v 5
提示：高对比度超现实拼贴画 --v 5_** |
| --- | --- |

---

## 型号版本 4

Midjourney V4 是 2022 年 11 月至 2023 年 5 月期间的默认模型。该模型具有由 Midjourney 设计的全新代码库和全新 AI 架构，并在新的 Midjourney AI 超级集群上进行训练。与之前的模型相比，模型版本 4 增加了对生物、地点和物体的了解。
该模型具有非常高的一致性，并且擅长图像提示。

| ![image.png](https://i.midjourney.xin//e91d25f2de2bb21d84eb3047482cdebd.png)
**_Prompt: vibrant California poppies
提示：充满活力的加州罂粟花_** | ![image.png](https://i.midjourney.xin//0722fe5f681de863fe2fe075ad98c4fc.png)
**_Prompt: high contrast surreal collage
提示：高对比度超现实拼贴_** |
| --- | --- |

---

## Niji Model 5

Niji 模型是 Midjourney 和 Spellbrush 之间的合作，旨在制作动漫和插图风格，并具有更多的动漫、动漫风格和动漫美学知识。它非常适合动态和动作镜头以及以角色为中心的构图。
要使用此模型，请将 `--niji 5` 参数添加到提示末尾，或使用 `/settings` 命令并选择 `🍏 Niji version 5`
该模型对 `--stylize` 参数敏感。尝试不同的风格化范围来微调您的图像。

### Niji 参数

Niji 模型版本 5 还可以使用 `--style` 参数进行微调，以获得独特的外观。尝试 `--style cute` 、`--style scenic` 、 `--style original` （使用原始 Niji 模型版本 5，这是 2023 年 5 月 26 日之前的默认值）或 `--style expressive`

|

##### **default --niji 5**

![image.png](https://i.midjourney.xin//db6490ff589bb9f7b4f23eefc371adf1.png)
birds perching on a twig --niji 5
栖息在树枝上的鸟儿——niji 5 |

##### **--style original**

![image.png](https://i.midjourney.xin//e2343410659a4002b30c29c051ad862a.png)
birds perching on a twig --niji 5 --style original
鸟儿栖息在树枝上 --niji 5 --风格原创 |
| --- | --- |

|

##### **--style cute**

![image.png](https://i.midjourney.xin//220ad0e0c0b628eaa5383f3e4892b2f4.png)
birds perching on a twig --niji 5 --style cute
栖息在树枝上的鸟儿 --niji 5 --风格可爱 |

##### **--style expressive**

![image.png](https://i.midjourney.xin//8c0de5fee8d53171dae04545e1b6bd19.png)
birds perching on a twig --niji 5 --style expressive
栖息在树枝上的鸟儿 --niji 5 --风格表现力 |

##### **--style scenic**

![image.png](https://i.midjourney.xin//0e49fa939118f632cbe126a7aa616b12.png)
birds perching on a twig --niji 5 --style scenic
栖息在树枝上的鸟儿--niji 5--风格风景 |
| --- | --- | --- |

### Niji 5 与 Midjourney 版本 5.2

|

##### **--v 5.2**

![image.png](https://i.midjourney.xin//9e2958ab057f395ddd9f7593cfff8112.png)
vibrant California poppies
充满活力的加州罂粟花 |

##### **--niji 5**

![image.png](https://i.midjourney.xin//86dce09dca07081f518c6e3aeb63c87d.png)
vibrant California poppies --niji 5
充满活力的加州罂粟花 --niji 5 |

##### **--v 5.2**

![image.png](https://i.midjourney.xin//216aa75f9c914baff66b933cf067664e.png)
birds sitting on a twig
鸟儿坐在树枝上 |
| --- | --- | --- |
|

##### **--niji 5**

![image.png](https://i.midjourney.xin//cc2b2bbde6879696b97e1e8f1d984739.png)
birds sitting on a twig --niji 5
鸟儿坐在树枝上——niji 5 | | |

---

## 如何切换型号

### 使用版本或测试参数

添加 `--v 4` `--v 5` `--v 5.1` `--v 5.1 --style raw` `--v 5.2` `--v 5.2 --style raw` `--niji 5` `--niji 5 --style cute` `--niji 5 --style expressive` `--niji 5 --style original` 或 `--niji 5 --style scenic` 到提示符末尾。
![](https://i.midjourney.xin//90138d8888b57c671972ffd63a4e964d.gif)

### 使用设置命令

输入 `/settings` 并从菜单中选择您喜欢的版本。
`4️⃣ MJ Version 4` `5️⃣ MJ Version 5` `5️⃣ MJ Version 5.1` `5️⃣ MJ Version 5.2` `🍎 Niji Version 5`

### 旧型号

您可以使用 `--version` 参数试验早期的 Midjourney 模型 了解有关 Midjourney 旧模型的更多信息。
