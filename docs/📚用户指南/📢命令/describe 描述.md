#### The **/describe** command allows you to upload an image and generate four possible prompts based on that image. Use the **/describe** command to explore new vocabulary and aesthetic movements.

**/describe** 命令允许您上传图像并根据该图像生成四种可能的提示。使用 **/describe** 命令探索新词汇和美学运动。
**/describe** generates prompts that are inspirational and suggestive, it cannot be used to recreate an uploaded image exactly.
**/describe** 生成具有启发性和暗示性的提示，它不能用于准确地重新创建上传的图像。
**/describe** returns the aspect ratio for uploaded images.
**/describe** 返回上传图像的宽高比。
